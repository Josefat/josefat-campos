public class PersonaContable extends Persona{
	public char CuentaContable;
	public int ClabeInterbancaria;
	public String Banco;
	public int CuentaBancaria;
	
	public PersonaContable(int Clave, String Nombres, String Apellidos,char CuentaContable, int ClabeInterbancaria, String Banco, int CuentaBancaria){
			super(Clave, Nombres, Apellidos);
			
			this.CuentaContable = CuentaContable;
			this.ClabeInterbancaria = ClabeInterbancaria;
			this.Banco = Banco;
			this.CuentaBancaria = CuentaBancaria;
	}
	
	public int getCuentaContable(){
		return CuentaContable;
	}
	
	public int getClabeInterbancaria(){
		return ClabeInterbancaria;
	}
	
	public String getBanco(){
		return Banco;
	}
	
	public int getCuentaBancaria(){
		return CuentaBancaria;
	}
}
	