public class Empleado extends PersonaContable{
	public String Puesto;
	public double SueldoBase;
	public int Bono;

	public Empleado(int Clave, String Nombres, String Apellidos,char CuentaContable, int ClabeInterbancaria, String Banco, int CuentaBancaria, String Puesto, double SueldoBase, int Bono){
		super(Clave, Nombres, Apellidos, CuentaContable, ClabeInterbancaria, Banco, CuentaBancaria);
		this.Puesto = Puesto;
		this.SueldoBase = SueldoBase;
		this.Bono = Bono;
	}
	
	public String getPuesto(){
		return Puesto;
	}
	
	public double getSueldoBase(){
		return SueldoBase;
	}
	
	public int getBono(){
		return Bono;
	}

}