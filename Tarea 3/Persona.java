public class Persona{
	public int Clave;
	public String Nombres;
	public String Apellidos;

	public Persona(int Clave, String Nombres, String Apellidos){
		this.Clave=Clave;
		this.Nombres=Nombres;
		this.Apellidos=Apellidos;
	}
	public int getClave(){
		return Clave;
	}
	public String getNombres(){
		return Nombres;
	}
	public String getApellidos(){
		return Apellidos;
	}
}