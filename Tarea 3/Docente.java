public class Docente extends Empleado{
	public String TipoDocente;
	public String GradoAcademico;
	
	public Docente(int Clave, String Nombres, String Apellidos,char CuentaContable, int ClabeInterbancaria, String Banco, int CuentaBancaria, String Puesto, double SueldoBase, int Bono, String TipoDocente, String GradoAcademico){
		super(Clave, Nombres, Apellidos, CuentaContable, ClabeInterbancaria, Banco, CuentaBancaria, Puesto, SueldoBase, Bono);
		
		this.TipoDocente = TipoDocente;
		this.GradoAcademico = GradoAcademico;
	}
	
	public String TipoDocente(){
		return TipoDocente;
	}
	
	public String GradoAcademico(){
		return GradoAcademico;
	}
}