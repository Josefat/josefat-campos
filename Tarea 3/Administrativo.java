import java.util.*;

public class Administrativo extends Empleado{
	public String TipoAdministrativo;
	public Date Antiguedad;

	public Administrativo(int Clave, String Nombres, String Apellidos,char CuentaContable, int ClabeInterbancaria, String Banco, int CuentaBancaria, String Puesto, double SueldoBase, int Bono, String TipoAdministrativo, Date Antiguedad){
		super(Clave, Nombres, Apellidos, CuentaContable, ClabeInterbancaria, Banco, CuentaBancaria, Puesto, SueldoBase, Bono);
		
		this.TipoAdministrativo= TipoAdministrativo;
		this.Antiguedad = Antiguedad;
	}
	
	public String getTipoAdministrativo(){
		return TipoAdministrativo;
	}
	
	public Date getAntiguedad(){
		return Antiguedad;
	}
	
}
